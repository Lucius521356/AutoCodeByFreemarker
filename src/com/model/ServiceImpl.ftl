package com.fps.web.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.fpswork.core.exception.exception.BusinessException;
import com.fps.common.Constants;
import com.fps.web.dao.${table_name}Dao;
import com.fps.web.dto.${table_name}DTO;
import com.fps.web.model.${table_name};
import com.fps.web.service.${table_name}Service;
@Service
@Transactional
public class ${table_name}ServiceImpl implements ${table_name}Service{
    
    @Autowired
    ${table_name}Dao ${table_name?uncap_first}Dao;
    public List<${table_name}> query${table_name}List(${table_name}DTO ${table_name?uncap_first}dto)  throws BusinessException{
    	return ${table_name?uncap_first}Dao.query${table_name}List(${table_name?uncap_first}dto);
    }
	public ${table_name} add${table_name}(${table_name} ${table_name?uncap_first})  throws BusinessException{
		return ${table_name?uncap_first}Dao.add${table_name}(${table_name?uncap_first});
	}
	public ${table_name} query${table_name}ByID(String id)  throws BusinessException{
		return ${table_name?uncap_first}Dao.query${table_name}ByID(id);
	}
	public Boolean delete${table_name}(${table_name} ${table_name?uncap_first})  throws BusinessException{
		return ${table_name?uncap_first}Dao.delete${table_name}(${table_name?uncap_first});
	}
}
