package com.fps.web.service;

import java.util.List;
import com.fpswork.core.exception.exception.BusinessException;
import com.fps.web.model.${table_name};
import com.fps.web.dto.${table_name}DTO;

/**
* @ClassName: ${table_name}Service
* @Description:${table_annotation}
* @author ${author}
* @date ${date}
*/
public interface ${table_name}Service{
	/**
	 * 获取所有${table_annotation}
	 */
	public List<${table_name}> query${table_name}List(${table_name}DTO ${table_name?uncap_first}dto)  throws BusinessException;
	/**
	 * 根据主键获取${table_annotation}
	 */
	public ${table_name} query${table_name}ByID(String id)  throws BusinessException;
    /**
	 * 保存${table_annotation}
	 */
	public ${table_name} add${table_name}(${table_name} ${table_name?uncap_first})  throws BusinessException;
	/**
	 * 删除${table_annotation}
	 */
	public Boolean delete${table_name}(${table_name} ${table_name?uncap_first})  throws BusinessException;
}







