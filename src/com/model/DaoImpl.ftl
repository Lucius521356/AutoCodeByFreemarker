package com.fps.web.dao.impl;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.fpswork.core.exception.exception.BusinessException;
import com.fps.utils.BaseDao;
import com.fpswork.util.BeanHelper;
import com.fpswork.util.StringUtil;
import com.fps.web.dao.${table_name}Dao;
import com.fps.web.dto.${table_name}DTO;
import com.fps.web.model.${table_name};
@Repository 
public class ${table_name}DaoImpl extends BaseDao implements ${table_name}Dao{
    
	public List<${table_name}> query${table_name}List(${table_name}DTO ${table_name?uncap_first}dto)  throws BusinessException{
		String sql="select * from ${table_name_small}";
			if(!"no".equals(${table_name?uncap_first}dto.getIslimit()))
			sql +=" LIMIT "+(Integer.parseInt(${table_name?uncap_first}dto.getPage())-1)*Integer.parseInt(${table_name?uncap_first}dto.getLimit())+","+${table_name?uncap_first}dto.getLimit()+"";
		return  this.findBySql(sql,${table_name}.class);
	}
	public ${table_name} query${table_name}ByID(String id)  throws BusinessException{
		return  this.get(${table_name}.class, id);
	}
	public ${table_name} add${table_name}(${table_name} ${table_name?uncap_first})  throws BusinessException{
		${table_name} ${table_name?uncap_first}1= this.get(${table_name}.class, ${table_name?uncap_first}.getId());
		if(StringUtil.isEmpty(${table_name?uncap_first}.getId())){
			this.save(${table_name?uncap_first});
		}else{
			BeanHelper.copyProperties(${table_name?uncap_first}1, ${table_name?uncap_first});
			this.update(${table_name?uncap_first}1);
		}
		
		return ${table_name?uncap_first}1;
	}
	public Boolean delete${table_name}(${table_name} ${table_name?uncap_first})  throws BusinessException{
		this.delete(this.get(${table_name}.class, ${table_name?uncap_first}.getId()));
		
		return true;
	}
}
