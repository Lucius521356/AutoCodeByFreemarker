package com.fps.web.controller.${table_name?lower_case};
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.fps.web.model.${table_name};
import com.fps.web.service.${table_name}Service;
import com.fpswork.util.JSONHelper;
import com.fpswork.util.StringUtil;
import com.fps.web.controller.BaseController;

/**
 * ${table_annotation}页面
 *
 */
@Controller
@RequestMapping("/${table_name?lower_case}")
public class ${table_name}Controller extends BaseController{
    
    @Autowired
    ${table_name}Service ${table_name?uncap_first}Service;
    
    
    /**
	 * 跳转${table_annotation}List界面
	 */
    @RequestMapping("/${table_name?lower_case}list")
    public ModelAndView ${table_name?lower_case}list() throws Exception{
    	ModelAndView modelAndView = new ModelAndView();   
    	//BaseUser baseUser = new BaseUser();
    	//modelAndView.addObject("userMap",JSONHelper.parseArray(baseUserService.queryUserList(baseUser)));
		modelAndView.setViewName("web/${table_name?lower_case}/${table_name?lower_case}list");      
        return modelAndView;
    }
   
    /**
	 * 加载List数据
	 */
    @RequestMapping(value = "/load${table_name}List", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> load${table_name}List(@ModelAttribute("${table_name}DTO")${table_name}DTO ${table_name?uncap_first}dto) throws Exception{
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("code", 0);
    	map.put("msg", "");
    	map.put("count", 1000);
    	map.put("data", JSONHelper.parseArray(${table_name?uncap_first}Service.query${table_name}List(${table_name?uncap_first}dto)));
        return map;
    }
    
    /**
   	 * ${table_annotation}管理
   	 */
    @RequestMapping("/${table_name?lower_case}Management")
    public ModelAndView ${table_name?lower_case}Management(String id,String type) throws Exception{
    	ModelAndView modelAndView = new ModelAndView();   
    	${table_name} ${table_name?lower_case}= new ${table_name}();
    	${table_name?lower_case} = ${table_name?uncap_first}Service.query${table_name}ByID(id);
    	modelAndView.addObject("${table_name?lower_case}",${table_name?lower_case});
    	modelAndView.addObject("type", type);
		modelAndView.setViewName("web/${table_name?lower_case}/${table_name?lower_case}management");      
        return modelAndView;
    }
    
    /**
   	 * ${table_annotation}管理提交表单
   	 */
    @RequestMapping(value = "/${table_name?lower_case}ManagementSubmit", method = RequestMethod.POST)
    public @ResponseBody Map<String, String> ${table_name?lower_case}ManagementSubmit(@ModelAttribute("${table_name}")${table_name} ${table_name?uncap_first}) throws Exception{
    	${table_name?uncap_first}Service.add${table_name}(${table_name?uncap_first});
    	Map<String, String> map = new HashMap<String, String>();
    	map.put("flag", "true");
        return map;
    }
    
    /**
   	 * ${table_annotation}删除
   	 */
    @RequestMapping(value = "/delete${table_name}", method = RequestMethod.POST)
    public @ResponseBody Map<String, String> delete${table_name}(@ModelAttribute("${table_name}")${table_name} ${table_name?uncap_first}) throws Exception{
    	${table_name?uncap_first}Service.delete${table_name}(${table_name?uncap_first});
    	Map<String, String> map = new HashMap<String, String>();
		map.put("flag", "true");
        return map;
    }
    

}
