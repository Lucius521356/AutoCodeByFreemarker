<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="utf-8">
  <title>${table_name}维护</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
</head>
<!--  1、 框架标签引入区 （start）-->
<!--  1、 框架标签引入区 （end）-->
<!--  2、 CSS声明区 （start）-->
<!--  2.0、引入CSS区 （start）-->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/other/layui/css/layui.css"/>
  	<link rel="stylesheet" href="<%=request.getContextPath() %>/freeStyle/css/saber.css"/>
<!--  2.0、 引入CSS区 （end）-->
<!--  2.1、本界面CSS区 （start）-->
<style type="text/css">
  <c:if test="${r'$'}{type=='detail'}">
	.layui-input{
			border: 0;
			background-color: rgba(255, 255, 255, 0);
	}
	</c:if>
</style>
<!--  2.1、 本界面CSS区 （end）-->
<!--  2、 CSS声明区 （end）-->
<!--  3、JS声明区 （start）-->
<!--  3.0、 导入外部JS区 （start）-->
<!--  3.0、 导入外部JS区 （end）-->
<!--  3.1、 全局变量声明区 （start）-->
<script type="text/javascript">

</script>
<!--  3.1、 全局变量声明区 （end）--> 
<!--  3.2、 初始化区 （start）-->
<script type="text/javascript">

</script>
<!--  3.2、 初始化区 （end）-->
<!--  3.3、 页面控制区 （start）-->
<script type="text/javascript">

</script>
<!--  3.3、 页面控制区 （end）-->
<!--  3.4、 事件区 （start）-->
<script type="text/javascript">


</script>
<!--  3.4、 事件区 （end）-->
<!--  3.5、 回调区 （start）-->
<script type="text/javascript">
window.onload=function(){ 
    $(".type").val($("#typeselect").val());
    $(".status").val($("#statusselect").val());
    layui.form.render('select');
}
</script>
<!--  3.5、 回调区 （end）-->
<!--  3.6、 业务逻辑区 （start）-->
<!--  3.6、 业务逻辑区 （end）-->
<!--  3、 JS声明区 （end）-->
<body class="saber3body">
<form class="layui-form" action="" style="margin:50px 50px 50px 50px">
<input name="id" value="${r'$'}{${table_name?lower_case}.id }"  type="hidden"/>
	<#if model_column?exists>
        <#list model_column as model>
<div class="layui-row">
	<div class="layui-col-xs6 layui-col-sm6 layui-col-md4"><div class="layui-form-item"></div></div>
    <div class="layui-col-xs6 layui-col-sm6 layui-col-md4">
      	<div class="layui-form-item">
    		<label class="layui-form-label">${model.columnComment!}：</label>
    		<div class="layui-input-block">
    		   	<#if (model.columnType = 'VARCHAR' || model.columnType = 'TEXT')>
      			<input type="text" name="${model.changeColumnName?lower_case}" lay-verify="title" value="${r'$'}{${table_name?lower_case}.${model.changeColumnName?lower_case} }" autocomplete="off" placeholder="请输入${model.columnComment!}" class="layui-input">    			</#if>
    			<#if model.columnType = 'DATETIME' >
    			<input type="text" name="${model.changeColumnName?lower_case}" id="${model.changeColumnName?lower_case}" lay-verify="date" value="<fmt:formatDate value="${r'$'}{${table_name?lower_case}.${model.changeColumnName?lower_case} }" pattern="yyyy-MM-dd"/>" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input">
    			</#if>
    		</div>
  		</div>
    </div>
</div>
	   </#list>
    </#if>
  <c:if test="${r'$'}{type!='detail'}">
	<div class="layui-row">
  		<div class="layui-col-xs6 layui-col-sm6 layui-col-md4"><div class="layui-form-item"></div></div>
  		<div class="layui-col-xs6 layui-col-sm6 layui-col-md4">
  		<div class="layui-form-item">
    	<div class="layui-input-block">
      		<button class="layui-btn" lay-submit="" lay-filter="${table_name?lower_case}add">保存</button>
      		<button type="reset" class="layui-btn layui-btn-primary">重置</button>
    	</div>
  		</div>
  		</div>
	</div>
</c:if>
</form>
</body>
<script type="text/javascript" src="<%=request.getContextPath() %>/other/layui/layui.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/common/jquery-2.0.3.min.js"></script>
<script type="text/javascript">
layui.use(['form', 'layedit', 'laydate','layer'], function(){
	  var $ = layui.jquery
	  ,form = layui.form
	  ,layer = layui.layer
	  ,layedit = layui.layedit
	  ,laydate = layui.laydate;
	<#if model_column?exists>
        <#list model_column as model>
    	<#if model.columnType = 'DATETIME' >
  		//日期
	  	laydate.render({
	    	elem: '#${model.changeColumnName?lower_case}'
	  	});    	
	  	</#if>
	   </#list>
    </#if>
	  
	  //监听提交
	  form.on('submit(${table_name?lower_case}add)', function(data){
		  $.ajax({
              url:"<%=request.getContextPath() %>/${table_name?lower_case}/${table_name?lower_case}ManagementSubmit.html",
              type:"post",
              async: false,
              data:data.field,
              success:function(res){
			   		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
		   	  		parent.layer.close(index);
              },
              error:function(e){
            	  layer.alert("错误！");
              }
          });  
	  });
	  
	});
</script>
</body>
</html>