package com.creat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.db.ColumnClass;

import freemarker.template.Template;

public class creat {
	

    private  String AUTHOR = "Spf";
    private  String CURRENT_DATE = "2017/11/09";
    private  String tableName = "Base_menu";
    private  String packageName = "com";
    private  String tableAnnotation = "菜单";
    private  String URL = "jdbc:mysql://127.0.0.1:3306/fps";
    private  String USER = "root";
    private  String PASSWORD = "root";
    private  String DRIVER = "com.mysql.jdbc.Driver";
    private  String changeTableName = replaceUnderLineAndUpperCase(tableName);
    private  String diskPath = "D://table/"+changeTableName+"/";

    public Connection getConnection() throws Exception{
        Class.forName(DRIVER);
        Connection connection= DriverManager.getConnection(URL, USER, PASSWORD);
        return connection;
    }
    @Test
    public  void test() throws Exception{
    	creat codeGenerateUtils = new creat();
        codeGenerateUtils.generate(AUTHOR,CURRENT_DATE,tableName,tableAnnotation,URL,USER,PASSWORD,diskPath);
    }

    public  void generate(String author,String date,String tablename,String tableannotation,String url,String user,String password,String path) throws Exception{
    	AUTHOR=author;
    	CURRENT_DATE=date;
    	tableName=tablename;
    	tableAnnotation=tableannotation;
    	URL=url;
    	USER=user;
    	PASSWORD=password;
    	changeTableName = replaceUnderLineAndUpperCase(tableName);
    	diskPath=path;
        try { 
            Connection connection = getConnection();
            DatabaseMetaData databaseMetaData = connection.getMetaData();
            ResultSet resultSet = databaseMetaData.getColumns(null,"%", tableName,"%");
            generateManagementJsp(resultSet);
            resultSet = databaseMetaData.getColumns(null,"%", tableName,"%");
            generateDTOFile(resultSet);
            resultSet = databaseMetaData.getColumns(null,"%", tableName,"%");
            generateListJsp(resultSet);
            resultSet = databaseMetaData.getColumns(null,"%", tableName,"%");
            generateModelFile(resultSet);
            generateDaoImplFile(resultSet);
            generateDaoFile(resultSet);
        
            generateServiceImplFile(resultSet);
            generateServiceFile(resultSet);
            generateControllerFile(resultSet);
       
        } catch (Exception e) {
            throw new RuntimeException(e);
        }finally{

        }
    }

    private void generateModelFile(ResultSet resultSet) throws Exception{

        final String suffix = ".java";
        String path = diskPath+"/bean/";
        

    }
    private void generateDTOFile(ResultSet resultSet) throws Exception{
        final String suffix = "DTO.java";
        String path = diskPath+"/dto/";;
        File fl = new File(path);
        
        if ( ! (fl.exists())  &&   ! (fl.isDirectory()))  {
       	 fl.mkdirs();
        } 
       path= path+ changeTableName + suffix;
       final String templateName = "DTO.ftl";
       File mapperFile = new File(path);
       List<ColumnClass> columnClassList = new ArrayList<>();
       ColumnClass columnClass = null;
       while(resultSet.next()){
           if(resultSet.getString("COLUMN_NAME").equals("id")) continue;
           columnClass = new ColumnClass();
           columnClass.setColumnName(resultSet.getString("COLUMN_NAME"));
           columnClass.setColumnType(resultSet.getString("TYPE_NAME"));
           columnClass.setChangeColumnName(replaceUnderLineAndUpperCase(resultSet.getString("COLUMN_NAME")));
           columnClass.setColumnComment(resultSet.getString("REMARKS"));
           columnClassList.add(columnClass);
       }
       Map<String,Object> dataMap = new HashMap<>();
       dataMap.put("model_column",columnClassList);
       generateFileByTemplate(templateName,mapperFile,dataMap);

    }
    
    private void generateDaoImplFile(ResultSet resultSet) throws Exception{
        final String suffix = "DaoImpl.java";
        String path = diskPath+"/dao/impl/";;
        File fl = new File(path);
         
         if ( ! (fl.exists())  &&   ! (fl.isDirectory()))  {
        	 fl.mkdirs();
         } 
        path= path+ changeTableName + suffix;
        final String templateName = "DaoImpl.ftl";
        File mapperFile = new File(path);
        Map<String,Object> dataMap = new HashMap<>();
        generateFileByTemplate(templateName,mapperFile,dataMap);
    }
    
    private void generateDaoFile(ResultSet resultSet) throws Exception{
        final String suffix = "Dao.java";
        String path = diskPath+"/dao/";;
        File fl = new File(path);
         
         if ( ! (fl.exists())  &&   ! (fl.isDirectory()))  {
        	 fl.mkdirs();
         } 
        path= path+ changeTableName + suffix;
        final String templateName = "Dao.ftl";
        File mapperFile = new File(path);
        Map<String,Object> dataMap = new HashMap<>();
        generateFileByTemplate(templateName,mapperFile,dataMap);

    }
 

    private void generateServiceImplFile(ResultSet resultSet) throws Exception{
        final String suffix = "ServiceImpl.java";
        String path = diskPath+"/service/impl/";;
        File fl = new File(path);
         
         if ( ! (fl.exists())  &&   ! (fl.isDirectory()))  {
        	 fl.mkdirs();
         } 
        path= path+ changeTableName + suffix;
        final String templateName = "ServiceImpl.ftl";
        File mapperFile = new File(path);
        Map<String,Object> dataMap = new HashMap<>();
        generateFileByTemplate(templateName,mapperFile,dataMap);
    }
    

    private void generateServiceFile(ResultSet resultSet) throws Exception{
        final String suffix = "Service.java";
        String path = diskPath+"/service/";;
        File fl = new File(path);
         
         if ( ! (fl.exists())  &&   ! (fl.isDirectory()))  {
        	 fl.mkdirs();
         } 
        path= path+ changeTableName + suffix;
        final String templateName = "ServiceInterface.ftl";
        File mapperFile = new File(path);
        Map<String,Object> dataMap = new HashMap<>();
        generateFileByTemplate(templateName,mapperFile,dataMap);
    }
    
    private void generateControllerFile(ResultSet resultSet) throws Exception{
        final String suffix = "Controller.java";
        String path = diskPath+"/controller/"+changeTableName.toLowerCase()+"/";
        File fl = new File(path);
         
         if ( ! (fl.exists())  &&   ! (fl.isDirectory()))  {
        	 fl.mkdirs();
         } 
        path= path+ changeTableName + suffix;
        final String templateName = "Controller.ftl";
        File mapperFile = new File(path);
        Map<String,Object> dataMap = new HashMap<>();
        generateFileByTemplate(templateName,mapperFile,dataMap);
    }
    private void generateManagementJsp(ResultSet resultSet) throws Exception{
        final String suffix = "management.jsp";
        String path = diskPath+"/jsp/"+changeTableName.toLowerCase()+"/";
        File fl = new File(path);
        
        if ( ! (fl.exists())  &&   ! (fl.isDirectory()))  {
       	 fl.mkdirs();
        } 
       path= path+ changeTableName.toLowerCase() + suffix;
       final String templateName = "ManagementJsp.ftl";
       File mapperFile = new File(path);
       List<ColumnClass> columnClassList = new ArrayList<>();
       ColumnClass columnClass = null;
       while(resultSet.next()){
           if(resultSet.getString("COLUMN_NAME").equals("id")) continue;
           columnClass = new ColumnClass();
           columnClass.setColumnName(resultSet.getString("COLUMN_NAME"));
           columnClass.setColumnType(resultSet.getString("TYPE_NAME"));
           columnClass.setChangeColumnName(replaceUnderLineAndUpperCase(resultSet.getString("COLUMN_NAME")));
           columnClass.setColumnComment(resultSet.getString("REMARKS"));
           columnClassList.add(columnClass);
       }
       Map<String,Object> dataMap = new HashMap<>();
       dataMap.put("model_column",columnClassList);
       generateFileByTemplate(templateName,mapperFile,dataMap);
    }
    private void generateListJsp(ResultSet resultSet) throws Exception{
        final String suffix = "list.jsp";
        String path = diskPath+"/jsp/"+changeTableName.toLowerCase()+"/";
        path= path+ changeTableName.toLowerCase() + suffix;
        final String templateName = "ListJsp.ftl";
        File mapperFile = new File(path);
        List<ColumnClass> columnClassList = new ArrayList<>();
        ColumnClass columnClass = null;
        while(resultSet.next()){
            if(resultSet.getString("COLUMN_NAME").equals("id")) continue;
            columnClass = new ColumnClass();
            columnClass.setColumnName(resultSet.getString("COLUMN_NAME"));
            columnClass.setColumnType(resultSet.getString("TYPE_NAME"));
            columnClass.setChangeColumnName(replaceUnderLineAndUpperCase(resultSet.getString("COLUMN_NAME")));
            columnClass.setColumnComment(resultSet.getString("REMARKS"));
            columnClassList.add(columnClass);
        }
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put("model_column",columnClassList);
        generateFileByTemplate(templateName,mapperFile,dataMap);
    }


    private void generateFileByTemplate(final String templateName,File file,Map<String,Object> dataMap) throws Exception{
        Template template = FreeMarkerTemplateUtils.getTemplate(templateName);
        FileOutputStream fos = new FileOutputStream(file);
        dataMap.put("table_name_small",tableName);
        dataMap.put("table_name",changeTableName);
        dataMap.put("author",AUTHOR);
        dataMap.put("date",CURRENT_DATE);
        dataMap.put("package_name",packageName);
        dataMap.put("table_annotation",tableAnnotation);
        Writer out = new BufferedWriter(new OutputStreamWriter(fos, "utf-8"),10240);
        template.process(dataMap,out);
    }

    public String replaceUnderLineAndUpperCase(String str){
        StringBuffer sb = new StringBuffer();
        sb.append(str);
        int count = sb.indexOf("_");
        while(count!=0){
            int num = sb.indexOf("_",count);
            count = num + 1;
            if(num != -1){
                char ss = sb.charAt(count);
                char ia = (char) (ss - 32);
                sb.replace(count , count + 1,ia + "");
            }
        }
        String result = sb.toString().replaceAll("_","");
        return result;
        //StringUtils.capitalize()
    }

}
